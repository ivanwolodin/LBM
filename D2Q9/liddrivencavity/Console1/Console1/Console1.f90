! computer code for lid-driven cavity
parameter (n=1000,m=40)
real f(0:8,0:n,0:m),uo,alpha,rhoo,dx,dy,omega
real feq(0:8,0:n,0:m),rho(0:n,0:m)
real strf(0:n,0:m)
real w(0:8), cx(0:8),cy(0:8)
real u(0:n,0:m), v(0:n,0:m)
integer i,j,kk

open(1,file="res.txt")
open(2,file="timeu")
open(3,file="Vx1")
open(4,file="Vy1")
!open(5, file="point velocity")

open(5,file='streamt.dat')
open(7,file='Vel.dat')
open(8,file='u.dat')
open(9,file='v.dat')

uo=0.2
sumvelo=0.0
rhoo=1.00
dx=1.0
dy=dx
dt=1.0
alpha=0.02

Re=uo*m/alpha
print *, "real Re=", Re

omega=1.0/(3.0*alpha+0.5d0)
print*, "omega= ", omega

mstep=5000

    w(0)=4./9.
	do i=1,4
		w(i)=1./9.
	end do
	do i=5,8
		w(i)=1./36.
	end do
	
cx(0)=0
cx(1)=1
cx(2)=0
cx(3)=-1
cx(4)=0
cx(5)=1
cx(6)=-1
cx(7)=-1
cx(8)=1
cy(0)=0
cy(1)=0
cy(2)=1
cy(3)=0
cy(4)=-1
cy(5)=1
cy(6)=1
cy(7)=-1
cy(8)=-1
	
	do j=0,m
		do i=0,n
			rho(i,j)=rhoo
			u(i,j)=0.0  !uo
			v(i,j)=0.0
		end do
	end do
	
	do j=21,m-1
	   u(0,j)=uo !4.0*uo/float(m)*j*(1.0-float(j)/float(m))
        !		v(i,m)=0.0
    end do
    
    do i=0,n
        u(i,0)=0
        u(i,m)=0
    enddo
    
    ! main loop
	do kk=1,mstep
		call collesion(u,v,f,feq,rho,omega,w,cx,cy,n,m)
		call streaming(f,n,m)		
		call sfbound(f,n,m,uo, u, v)
		call rhouv(f,rho,u,v,cx,cy,n,m)
!		IF (mod(kk,1000).eq.0) THEN
!            call intermediate_velocity_field(u,v,rho,uo,n,m,kk)
!       ENDIF
        IF  (mod(kk,100).eq.0) print *, "Step number", kk
    enddo
	! end of the main loop
    
! =================================
! Output
call result(u,v,rho,uo,n,m)

close(1)
close(2)
close(3)
close(4)
close(5)
close(7)
close(8)
close(9)

stop
end
! end of the main program    

subroutine collesion(u,v,f,feq,rho,omega,w,cx,cy,n,m)
real f(0:8,0:n,0:m)
real feq(0:8,0:n,0:m),rho(0:n,0:m)
real w(0:8), cx(0:8),cy(0:8)
real u(0:n,0:m), v(0:n,0:m),omega
	DO i=0,n
		DO j=0,m
			t1=u(i,j)*u(i,j)+v(i,j)*v(i,j)
			DO k=0,8
				t2=u(i,j)*cx(k)+v(i,j)*cy(k)
				feq(k,i,j)=rho(i,j)*w(k)*(1.0+3.0*t2+4.50*t2*t2-1.50*t1)
				f(k,i,j)=omega*feq(k,i,j)+(1.-omega)*f(k,i,j)
			END DO
		END DO
	END DO
return
end


subroutine streaming(f,n,m)
real f(0:8,0:n,0:m)
	! streaming
	DO j=0,m
	
		DO i=n,1,-1 !RIGHT TO LEFT
			f(1,i,j)=f(1,i-1,j)
		END DO
	
		DO i=0,n-1 !LEFT TO RIGHT
			f(3,i,j)=f(3,i+1,j)
		END DO
	
	END DO

	DO j=m,1,-1 !TOP TO BOTTOM
		DO i=0,n
			f(2,i,j)=f(2,i,j-1)
		END DO
		DO i=n,1,-1
			f(5,i,j)=f(5,i-1,j-1)
		END DO
		DO i=0,n-1
			f(6,i,j)=f(6,i+1,j-1)
		END DO
	END DO
	
	DO j=0,m-1 !BOTTOM TO TOP
		
		DO i=0,n
			f(4,i,j)=f(4,i,j+1)
		END DO
		
		DO i=0,n-1
			f(7,i,j)=f(7,i+1,j+1)
		END DO
		
		DO i=n,1,-1
			f(8,i,j)=f(8,i-1,j+1)
		END DO
	END DO
return
end


subroutine rhouv(f,rho,u,v,cx,cy,n,m)
real f(0:8,0:n,0:m),rho(0:n,0:m),u(0:n,0:m),v(0:n,0:m),cx(0:8),cy(0:8)
real ssum,usum,vsum
	do j=0,m
		do i=0,n
			ssum=0.0
				do k=0,8
					ssum=ssum+f(k,i,j)
				end do
			rho(i,j)=ssum
		end do
	end do

       ! do i=1,n
       !    rho(i,m)=f(0,i,m)+f(1,i,m)+f(3,i,m)+2.*(f(2,i,m)+f(6,i,m)+f(5,i,m))
       ! end do

        DO i=1,n
		DO j=1,m-1
			usum=0.0
			vsum=0.0
				DO k=0,8
					usum=usum+f(k,i,j)*cx(k)
					vsum=vsum+f(k,i,j)*cy(k)
				END DO
			u(i,j)=usum/rho(i,j)
			v(i,j)=vsum/rho(i,j)
		END DO
	END DO
	
	!do j=1,m
        !   v(n,j)=0.0
        !   u(n,j)=0.0
   	!enddo


    do j=0,20
      do i=0,40
       v(i,j)=0.0
       u(i,j)=0.0
      enddo
    enddo
   do j=1,m
       v(n,j)=0.0
   enddo

return
end


subroutine sfbound(f,n,m,uo, u, v)
real f(0:8,0:n,0:m),uo,rhow
real u(0:n,0:m),v(0:n,0:m)


	! SQUARE OBSTACLE
	! =======================================================

!        do i=0,n
!            v(i,0)=0
!            u(i,m)=0
!        enddo

        do j=0,20
           !East
!           f(6,208,j) = f(8,208,j)
!           f(3,208,j) = f(1,208,j)
!           f(7,208,j) = f(5,208,j)

           f(5,40,j) = f(7,40,j)
           f(1,40,j) = f(3,40,j)
           f(8,40,j) = f(6,40,j)


           !West
!          f(5,192,j)=f(7,192,j)
!           f(1,192,j)=f(3,192,j)
!           f(8,192,j)=f(6,192,j)

!           f(6,600,j)=f(8,600,j)
!           f(3,600,j)=f(1,600,j)
!           f(7,600,j)=f(5,600,j)

       enddo

        do i=0,40
           !Top
!           f(7,i,72) = f(5,i,72)
!           f(4,i,72) = f(2,i,72)
!           f(8,i,72) = f(6,i,72)

           f(6,i,20) = f(8,i,20)
           f(2,i,20) = f(4,i,20)
           f(5,i,20) = f(7,i,20)


           !Bottom
!           f(6,i,56) = f(8,i,56)
!           f(2,i,56) = f(4,i,56)
!           f(5,i,56) = f(7,i,56)

           !f(7,i,168) = f(5,i,168)
           !f(4,i,168) = f(2,i,168)
           !f(8,i,168) = f(6,i,168)
        enddo

        ! =======================================================

        !bottom wall
        do i=0,n
           f(6,i,0) = f(8,i,0)
           f(2,i,0) = f(4,i,0)
           f(5,i,0) = f(7,i,0)
	enddo

	!top wall
	do i=0,n
           f(7,i,m) = f(5,i,m)
           f(4,i,m) = f(2,i,m)
           f(8,i,m) = f(6,i,m)
	enddo

        !Pumping energy from west wall
	do j=0,m
	   rhow = (1./(1-uo))*(f(0,0,j)+f(2,0,j)+f(4,0,j)+2*(f(3,0,j)+f(6,0,j)+f(7,0,j)))
	   f(1,0,j) = f(3,0,j) + 2.*rhow*uo/3.
	   f(5,0,j) = f(7,0,j) + rhow*uo/6.
	   f(8,0,j) = f(6,0,j) + rhow*uo/6.
	enddo
 !       do i=0,8
 !          print*, f(i,0,64)
 !       enddo
 !       pause
        ! known density, rho=1
!        do j=0,m
!           u(i,j) =-1.0 + (f(0,n,j)+f(2,n,j)+f(4,n,j)+2*(f(1,n,j)+f(5,n,j)+f(8,n,j)))/rhoo

!           f(3,n,j)= f(1,n,j) - (2.0*u(i,j)*rhoo)/3.0
 !          f(7,n,j)= f(5,n,j) - (u(i,j)*rhoo)/6.0
 !          f(6,n,j) = f(8,n,j) - (u(i,j)*rhoo)/6.0
 !       enddo
 
         ! ===============================
         ! PERIODIC
!         do j=0,m
            !f(1,n,j)  = f(1,0,j)
            !f(2,n,j)  = f(2,0,j)
!            f(3,n,j)  = f(3,0,j)
            !f(4,n,j)  = f(4,0,j)
            !f(5,n,j)  = f(5,0,j)
!            f(6,n,j)  = f(6,0,j)
!            f(7,n,j)  = f(7,0,j)
            !f(8,n,j)  = f(8,0,j)
!         enddo
         ! ===============================

         ! less velocities
         ! ===============================
        ! do j=0,m
        ! uoo=0.013
        !   rhoe = (1./(1+uoo))*(f(0,n,j)+f(2,n,j)+f(4,n,j)+2*(f(1,n,j)+f(5,n,j)+f(8,n,j)))
	!   f(3,0,j) = f(1,0,j) - 2.*rhoe*uoo/3.
	!   f(7,0,j) = f(5,0,j) - rhoe*uoo/6.
	!   f(6,0,j) = f(8,0,j) - rhoe*uoo/6.
        ! enddo
         ! ===============================

	! Constant flux
	 do j=1,m
	    f(1,n,j) =2.0d0*f(1,n-1,j)-f(1,n-2,j)

	!	   f(2,n,j) =2*f(2,n-1,j)-f(2,n-2,j)
	!	   f(3,n,j) = 2*f(3,n-1,j)-f(3,n-2,j)
	!	   f(4,n,j) = 2*f(4,n-1,j)-f(4,n-2,j)

	     f(5,n,j) = 2.0d0*f(5,n-1,j)-f(5,n-2,j)

	!	   f(6,n,j) = 2*f(6,n-1,j)-f(6,n-2,j)
	!	   f(7,n,j) = 2*f(7,n-1,j)-f(7,n-2,j)
		   !f(8,n,j) = f(8,n-1,j)

	      f(8,n,j) = 2.0d0*f(8,n-1,j)-f(8,n-2,j)
		  ! f(9,n,j) = 2*f(9,n-1,j)-f(9,n-2,j)

!		  f(6,n,j) =0.0


 !                  f(1,n,j)=f(1,0,j)
 !                  f(2,n,j)=f(2,0,j)
 !                  f(3,n,j)=f(3,0,j)
 !                  f(4,n,j)=f(4,0,j)
 !                  f(5,n,j)=f(5,0,j)
 !                  f(6,n,j)=f(6,0,j)
 !                  f(7,n,j)=f(7,0,j)
 !                  f(8,n,j)=f(8,0,j)
     enddo

	 do j=0,m-1
!		  f(3,n,j) =0.0
!		  f(7,n,j) =0.0
	 enddo
!	 f(3,n,m)=0.0

return
end


subroutine intermediate_velocity_field(u,v,rho,uo,n,m,kk)
real u(0:n,0:m),v(0:n,0:m)
real rho(0:n,0:m),strf(0:n,0:m)
!open(5,file="Vx1")
!open(6,file="Vy1")
 !write(1,*)"Step", kk
          do i=0,n
             do j=0,m
                !write(3, *) "i=", i
                !write(3,*) "j=", j
                 ! output by columns
                 write(1,*)(sqrt(v(i,j)*v(i,j)+u(i,j)*u(i,j)))
                 write(3,*)(u(i,j))
                 write(4,*)(v(i,j))
             enddo
          enddo
return
end


subroutine result(u,v,rho,uo,n,m)
real u(0:n,0:m),v(0:n,0:m)
real strf(0:n,0:m),rho(0:n,0:m)
!open(5,file='streamt.dat')

! streamfunction calculations
!strf(0,0)=0.
!    do i=0,n
        
!	    rhoav=0.5*(rho(i-1,0)+rho(i,0))
!	    if(i.ne.0) strf(i,0)=strf(i-1,0)-rhoav*0.5*(v(i-1,0)+v(i,0))
!	    do j=1,m
!	        rhom=0.5*(rho(i,j)+rho(i,j-1))
!	        strf(i,j)=strf(i,j-1)+rhom*0.5*(u(i,j-1)+u(i,j))
 !       end do
 !       
  !  end do
    
    ! output Ux, Uy
    do i=0,n
	    do j=0,m
	        write(8,*) u(i,j)
	        write(9,*) v(i,j)
	    end do
    end do
    
    ! output streamfunction
!	do i=0,n
!	    do j=0,m
!	        write(5,*) strf(i,j)
!	    enddo
!    enddo
    
    ! output module velocity
    do i=0,n
	    do j=0,m
	        write(7,*) sqrt(u(i,j)*u(i,j)+v(i,j)*v(i,j))
	    enddo
    enddo 

return
end
%% Spatial velocity evolution

Nx = 1001;
Ny = 41;
slice = 1;
Re = ' ';
%% Step_time=200
step_number = 1;
titl = int2str(step_number*slice);

Vel = VWhole(: , step_number);
W = reshape(Vel,Ny, []);
W = W./0.2;

% xlim([0 Nx])
figure(1)
plot(W(:,100), 'b')
hold on;

plot(W(:,200), 'r')
plot(W(:,999), 'black')
%plot(W(:,2390), 'g')
ylabel('V');
xlabel('Node');
titt_data = strcat('V(N), Re=' , Re)
titt_data = strcat(titt_data, ',  ');
titt_data = strcat(titt_data, 'Time=');
titl = strcat(titt_data, titl);
title(titl);
hleg1 = legend('100','200', '990');
%{
%% Step_time=6000
step_number = 30;
titl = int2str(step_number*slice);
figure(2)
Vel = VWhole(: , step_number);
W = reshape(Vel,Ny, []);
% xlim([0 Nx])

plot(W(:,180), 'b')
hold on;

plot(W(:,250), 'r')
plot(W(:,400), 'black')
plot(W(:,790), 'g')
ylabel('V');
xlabel('Node');
titt_data = strcat('V(N), Re=' , Re)
titt_data = strcat(titt_data, ',  ');
titt_data = strcat(titt_data, 'Time=');
titl = strcat(titt_data, titl);
title(titl);
hleg1 = legend('180','250', '400', '790');

%% Step_time=12000
step_number = 60;
titl = int2str(step_number*slice);

Vel = VWhole(: , step_number);
W = reshape(Vel,Ny, []);
%xlim([0 Nx])
figure(3)
plot(W(:,180), 'b')
hold on;

plot(W(:,250), 'r')
plot(W(:,400), 'black')
plot(W(:,790), 'g')
ylabel('V');
xlabel('Node');
titt_data = strcat('V(N), Re=' , Re)
titt_data = strcat(titt_data, ',  ');
titt_data = strcat(titt_data, 'Time=');
titl = strcat(titt_data, titl);
title(titl);
hleg1 = legend('180','250', '400', '790');

%{
%% Step_time=18000
step_number = 90;
titl = int2str(step_number*slice);
figure(4)
Vel = VWhole(: , step_number);
W = reshape(Vel,Ny, []);
% xlim([0 Nx])

plot(W(:,180), 'b')
hold on;

plot(W(:,250), 'r')
plot(W(:,400), 'black')
plot(W(:,790), 'g')
ylabel('V');
xlabel('Node');
titt_data = strcat('V(N), Re=' , Re)
titt_data = strcat(titt_data, ',  ');
titt_data = strcat(titt_data, 'Time=');
titl = strcat(titt_data, titl);
title(titl);
hleg1 = legend('180','250', '400', '790');

%% Step_time=24000
step_number = 120;
titl = int2str(step_number*slice);
figure(5)
Vel = VWhole(: , step_number);
W = reshape(Vel,Ny, []);
% xlim([0 Nx])

plot(W(:,180), 'b')
hold on;

plot(W(:,250), 'r')
plot(W(:,400), 'black')
plot(W(:,790), 'g')
ylabel('V');
xlabel('Node');
titt_data = strcat('V(N), Re=' , Re)
titt_data = strcat(titt_data, ',  ');
titt_data = strcat(titt_data, 'Time=');
titl = strcat(titt_data, titl);
title(titl);
hleg1 = legend('180','250', '400', '790');
%}

%%
% Horizontal Slice
%% Step_time=24000
step_number = 70;
slice=200
titl = int2str(step_number*slice);
figure(6)
Vel = VWhole(: , step_number);
W = reshape(Vel,Ny, []);
% xlim([0 Nx])

plot(W(20,:), 'b')
hold on;

plot(W(40,:), 'r')
plot(W(60, :), 'black')
plot(W(80, :), 'g')
ylabel('V');
xlabel('Node');
titt_data = strcat('V(N), Re=' , Re)
titt_data = strcat(titt_data, ',  ');
titt_data = strcat(titt_data, 'Time=');
titl = strcat(titt_data, titl);
title(titl);
hleg1 = legend('20','40', '60', '80');
%}
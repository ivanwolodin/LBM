clear;clc;
%% Arranging grid size
Nx = 1001;
Ny = 41;
Grid_Size = Nx*Ny;
x = 0:(Nx-1);
y = 0:(Ny-1);
[X,Y] = meshgrid(x,y);

%% read from file STREAM F

fileI = fopen('streamt.dat','r');
out = fscanf(fileI,'%f');
fclose(fileI);

%% resize read data
stream_function = reshape(out,Grid_Size, []);
out = stream_function(: , 1);
StreamFunc = reshape(out,Ny, []);

figure(4)
contour(X,Y,StreamFunc,90)
%contour(X,Y,StreamFunc,'ShowText','on')
title('Stream Line, Re=4')
hold on;
%%{
%% read from file VELOCITY
fileI = fopen('u.dat','r');
out = fscanf(fileI,'%f');
fclose(fileI);

%% resize read data
U = reshape(out,Grid_Size, []);
out = U(: , 1);
U = reshape(out,Ny, []);

%%

%% read from file VELOCITY
fileI = fopen('v.dat','r');
out = fscanf(fileI,'%f');
fclose(fileI);

%% resize read data
V = reshape(out,Grid_Size, []);
out = V(: , 1);
V = reshape(out,Ny, []);
discrete=10;
figure(100)
quiver(X(1:discrete:end),Y(1:discrete:end),U(1:discrete:end),V(1:discrete:end))
%%}
%{%}
%% Color Gradient 

fileI = fopen('Vel.dat','r');
out = fscanf(fileI,'%f');
fclose(fileI);

%% resize read data
stream_function = reshape(out,Grid_Size, []);
out = stream_function(: , 1);
VV = reshape(out,Ny, []);

figure(30)
imagesc(VV)


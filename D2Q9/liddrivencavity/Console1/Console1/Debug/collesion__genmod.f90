        !COMPILER-GENERATED INTERFACE MODULE: Mon Apr 01 10:33:18 2019
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE COLLESION__genmod
          INTERFACE 
            SUBROUTINE COLLESION(U,V,F,FEQ,RHO,OMEGA,W,CX,CY,N,M)
              INTEGER(KIND=4) :: M
              INTEGER(KIND=4) :: N
              REAL(KIND=4) :: U(0:N,0:M)
              REAL(KIND=4) :: V(0:N,0:M)
              REAL(KIND=4) :: F(0:8,0:N,0:M)
              REAL(KIND=4) :: FEQ(0:8,0:N,0:M)
              REAL(KIND=4) :: RHO(0:N,0:M)
              REAL(KIND=4) :: OMEGA
              REAL(KIND=4) :: W(0:8)
              REAL(KIND=4) :: CX(0:8)
              REAL(KIND=4) :: CY(0:8)
            END SUBROUTINE COLLESION
          END INTERFACE 
        END MODULE COLLESION__genmod

! computer code for lid-driven cavity two phase flow
parameter (n=10,m=7)

! *************************************************************************
! *************************************************************************
! Arrays:
real Force_x(0:n, 0:m), Force_y(0:n, 0:m)                             ! Fx, Fy
real uu(0:n,0:m), vv(0:n,0:m)                                         ! ux, uy
real rho_total(0:n, 0:m), rho_f1(0 : n, 0 : m), rho_f2(0 : n, 0 : m)  ! density

real strf(0:n, 0:m)                                                   ! stream function
real w(0:8), cx(0:8), cy(0:8), G(0:8)                                 ! normalization constants
real Psi(0:n,0:m)                                                     ! auxillary func.

real f1(0:8,0:n,0:m), feq1(0:8,0:n,0:m)                               ! Df1
real f2(0:8,0:n,0:m), feq2(0:8,0:n,0:m)                               ! Df2
real ff(0:1, 0:n, 0:m)                                                ! general Df

! ************
real mass(0:8, 0:n, 0:m)                                              ! mass of the cell


! *************************************************************************
! *************************************************************************

real u0                 ! characterestic velocity
real theta              ! reduced temperature

real rhoo_1 
real tau_1              ! relaxation time
real omega_1            ! collision frequency
real viscosity_1

real rhoo_2
real tau_2              ! relaxation time
real omega_2            ! collision frequency
real viscosity_2 


real speedofsound 
real dx, dy, dt

real alpha              ! normalizaion constant ??  alpha=1.5 for D2Q9
real acceleration_g     ! acceleration of gravity
integer i, j, step

print*,"*********************************"
print*,"*********************************"
print*, "Modeling of two-phase system"
print*,"*********************************"
print*,"*********************************"

steps_number=100
print*, "step number= ", steps_number

    w(0)=4./9.
	do i=1,4
		w(i)=1./9.
	end do
	do i=5,8
		w(i)=1./36.
	end do
	
    cx(0) =  0
    cx(1) =  1
    cx(2) =  0
    cx(3) =  -1
    cx(4) =  0
    cx(5) =  1
    cx(6) =  -1
    cx(7) =  -1
    cx(8) =  1
    
    cy(0) = 0
    cy(1) = 0
    cy(2) = 1
    cy(3) = 0
    cy(4) = -1
    cy(5) = 1
    cy(6) = 1
    cy(7) = -1
    cy(8) = -1
    
    call open_files()
    call initial_distribution(n, m, rho_total, uu, vv, Force_x, Force_y, speedofsound, cx, cy, rhoo_1, rhoo_2, &
                              u0, dx, dy, dt, alpha, viscosity_1, tau_1, omega_1, viscosity_2, tau_2, omega_2, theta, & 
                              mass, acceleration_g, f1, f2, rho_f1, rho_f2, Psi, alpha_k1, alpha_k2, ff, w)
    
    
    ! main loop
	do step=1, steps_number
        print*, 'step=', step
        call main_function(n, m, rho_total, uu, vv, Force_x, Force_y, speedofsound, cx, cy, rhoo_1, rhoo_2, u0, &
                           dx, dy, dt, theta, alpha, viscosity_1, omega_1, rho_f1, tau_1, viscosity_2, omega_2, tau_2, rho_f2, &
                           ff, f1, feq1, f2, feq2, step, mass, w, acceleration_g, Psi, alpha_k1, alpha_k2)  
    enddo
	! end of the main loop
    
! =================================
! Output
! call result(uu,vv,u0,n,m,rho)
call close_files()
stop
end
! end of the main program    

subroutine main_function   (n, m, rho_total, uu, vv, Force_x, Force_y, speedofsound, cx, cy, rhoo_1, rhoo_2, u0, &
                           dx, dy, dt, theta, alpha, viscosity_1, omega_1, rho_f1, tau_1, viscosity_2, omega_2, tau_2, rho_f2, &
                           ff, f1, feq1, f2, feq2, step, mass, w, acceleration_g, Psi, alpha_k1, alpha_k2)
integer step, i, j
real Force_x(0:n, 0:m), Force_y(0:n, 0:m)
real uu(0:n,0:m), vv(0:n,0:m)            
real rho_total(0:n, 0:m)                       
real Psi(0:n,0:m)    
real strf(0:n, 0:m)                      
real w(0:8), cx(0:8), cy(0:8)  

real mass(0:n,0:m), rho_f1(0:n,0:m), rho_f2(0:n,0:m)
real f1(0:8,0:n,0:m), feq1(0:8,0:n,0:m), f2(0:8,0:n,0:m), feq2(0:8,0:n,0:m), ff(0:1, 0:n, 0:m)
real rhoo_1, rhoo_2     
real u0                 
real theta, acceleration_g 
real viscosity_1, omega_1, tau_1, alpha_k1         
real viscosity_2, omega_2, tau_2, alpha_k2  
real speedofsound 
real dx, dy, dt
real alpha     

    call debug_info(Force_x, Force_y, rho_total, step, n, m, mass, uu, vv, f1, f2)
    
    call recoloring(ff, f1, f2, rhoo_1, rhoo_2, rho_total, cx, cy, alpha_k1, n, m, 1)
    call recoloring(ff, f1, f2, rhoo_1, rhoo_2, rho_total, cx, cy, alpha_k2, n, m, 2)
    
    call streaming(f1, n, m)	
    call streaming(f2, n, m)
    
    call psi_calculation(n, m, step, rho_f1, rho_f2, Psi)

    call collision(uu, vv, ff, f1, f2, feq1, feq2, rhoo_1, rhoo_2, omega_1, omega_2, w, cx, cy, n, m, &
                   theta, dt, step, substance_after_first_step, alpha_k1, alpha_k2, Psi)
    
    call boundary_conditions(f1, n, m, uu, vv) 
    call boundary_conditions(f2, n, m, uu, vv) 
    
    call force_calculation(n, m, Force_x, Force_y, step, dx, cx, cy, mass, acceleration_g, Psi, w, rho_total)
    call rho_and_velocity_calculation(f1, f2, rho_total, uu, vv ,cx, cy, n, m, Force_x, Force_y, step, dt)


return 
end

subroutine open_files()
    
   ! open(1,file="debug_info/Phi.dat")
    !open(2,file="debug_info/Potential U.dat")

    open(3,file="evolutionVx1.dat")
    open(4,file="evolutionVy1.dat")
    open(5,file='rho_evolution.dat')
    open(7,file='rho.dat')
    open(8,file='u.dat')
    open(9,file='v.dat')

   ! open(10,file='debug_info/Pressure P.dat')
   ! open(11,file='debug_info/Density rho.dat')
    !open(12,file='debug_info/Psi.dat')
  !  open(13,file='debug_info/Fx.dat')

    !open (14, file='debug_info/distribution fucntion f.dat')
print*, "Debug files"
    open(21,file='debug_info/Fx.dat')
    open(22,file='debug_info/Fy.dat')
    open(23,file='debug_info/eps.dat')
    open(24,file='debug_info/mass.dat')
    open(25,file='debug_info/u.dat')
    open(26,file='debug_info/v.dat')
    open(27, file='debug_info/distribution fucntion f.dat')
    print*, "Debug files"
    open(30,file='Distribution_function_debug/Df_0.dat')
    open(31,file='Distribution_function_debug/Df_1.dat')
    open(32,file='Distribution_function_debug/Df_2.dat')
    open(33,file='Distribution_function_debug/Df_3.dat')
    open(34,file='Distribution_function_debug/Df_4.dat')
    open(35,file='Distribution_function_debug/Df_5.dat')
    open(36,file='Distribution_function_debug/Df_6.dat')
    open(37,file='Distribution_function_debug/Df_7.dat')
    open(38,file='Distribution_function_debug/Df_8.dat')
    print*, "Debug files"
    open(40,file='Distribution_function2_debug/Df_2_0.dat')
    open(41,file='Distribution_function2_debug/Df_2_1.dat')
    open(42,file='Distribution_function2_debug/Df_2_2.dat')
    open(43,file='Distribution_function2_debug/Df_2_3.dat')
    open(44,file='Distribution_function2_debug/Df_2_4.dat')
    open(45,file='Distribution_function2_debug/Df_2_5.dat')
    open(46,file='Distribution_function2_debug/Df_2_6.dat')
    open(47,file='Distribution_function2_debug/Df_2_7.dat')
    open(48,file='Distribution_function2_debug/Df_2_8.dat')
    print*, "Debug files"
return
end

subroutine initial_distribution(n, m, rho_total, uu, vv, Force_x, Force_y, speedofsound, cx, cy, rhoo_1, rhoo_2, &
                                u0, dx, dy, dt, alpha, viscosity_1, tau_1, omega_1, viscosity_2, tau_2, omega_2, theta, &
                                mass, acceleration_g, f1, f2, rho_f1, rho_f2, Psi, alpha_k1, alpha_k2, ff, w)

real Force_x(0:n,0:m), Force_y(0:n, 0:m)
real f1(0:8,0:n,0:m), f2(0:8,0:n,0:m), ff(0:1,0:n,0:m), feq1(0:8,0:n,0:m), feq2(0:8,0:n,0:m)
real rho_total(0:n, 0:m), rho_f1(0:n, 0:m), rho_f2(0:n, 0:m)
real uu(0:n, 0:m), vv(0:n, 0:m)
real cx(0:8), cy(0:8)
real rhoo_1, rhoo_2, u0
integer i, j
real Psi(0:n,0:m)     
real mass(0:n, 0:m)      
real acceleration_g
real integral_mass
real alpha_k1, alpha_k2
real w(0:8)
real  C1(0  : 8), C2(0  : 8)

integral_mass = 0.0
acceleration_g = 0.006 ! 0.01! 1 !0.005
! *****************************


    ! ======================================================================
    ! INITIAL PARAMETERS
    ! ======================================================================
    
    u0=0.2
        
    dx=1.0
    dy=dx
    dt=1.0
    
    alpha=1.5 
    
    viscosity_1 = 0.02   ! air 
    omega_1 = 1.0/(3.0*viscosity_1+0.5d0)
    tau_1 = 1.0/omega_1
    
    viscosity_2 = 0.3  ! fluid
    omega_2 = 1.0/(3.0*viscosity_2+0.5d0)
    tau_2 = 1.0/omega_2
    
    theta = ((dx/dt)*(dx/dt))/3
    speedofsound = 1.0 / (sqrt(3.0))
    Re=u0*m/viscosity_1
    print*, "INPUT PARAMETERS:"
    print *, "Re=", Re
    print*, "omega_1 = ", omega_1
    print*, "omega_2 = ", omega_2
    print*, "Relaxation time:"
    print*, "tau_1 = ", tau_1
    print*, "tau_2 = ", tau_2
    print*, "speed = ", u0
    print*, "dx= ", dx, "dy=", dy, "dt=", dt
    print*, "reduced temperature theta=", theta
    
    !*************************************************************
    !*************************************************************
    !*****                                                   ***** 
    !*****                                                   *****
    !*****                                                   *****
    !*****                                                   *****
    !*****                                                   *****
    !*************************************************************
    !rho1/rho2 = 3 --->  rho1 = 3 * rho2    
    rhoo_1 =    0.09    ! air
    rhoo_2 =    0.03    ! fluid
    alpha_k1 = 0.8      ! 
    alpha_k2 = 0.4      !   
    
    C1(0) = alpha_k1
    C1(1) = (1-alpha_k1)/5.0
    C1(2) = (1-alpha_k1)/5.0
    C1(3) = (1-alpha_k1)/5.0
    C1(4) = (1-alpha_k1)/5.0
    C1(5) = (1-alpha_k1)/20.0
    C1(6) = (1-alpha_k1)/20.0
    C1(7) = (1-alpha_k1)/20.0
    C1(8) = (1-alpha_k1)/20.0

    C2(0) = alpha_k2
    C2(1) = (1-alpha_k2)/5.0
    C2(2) = (1-alpha_k2)/5.0
    C2(3) = (1-alpha_k2)/5.0
    C2(4) = (1-alpha_k2)/5.0
    C2(5) = (1-alpha_k2)/20.0
    C2(6) = (1-alpha_k2)/20.0
    C2(7) = (1-alpha_k2)/20.0
    C2(8) = (1-alpha_k2)/20.0
    
    Psi = 0.0
    uu = 0.0
    vv = 0.0
    print*, "Start?"
    pause
    do i = 0, n
        do j = 0, m
            rho_f1(i,j) = rhoo_1
        enddo
    enddo
    
    do i = 0, n/2
        do j = 0, m/2
            rho_f2(i,j) = rhoo_2
            rho_f1(i,j) = 0.0
        end do
    enddo
    
    do i = 0, n
        do j = 0, m
            rho_total(i,j) = ( rho_f1(i,j) + rho_f2(i,j) )
        end do
    enddo
    
    
    !rho_total(3,3) = rhoo_2
    !rho_f2(3, 3) = rhoo_2 
    
    ! velocity
  !  do i=0,n
  !      do j=0,m
  !          uu(i,j) = 0.0 !u0
  !          vv(i,j) = 0.0
  !      enddo
  !  enddo
    
    
    do i = 0, n
        do j = 0, m
            !p(i,j) = (rho_total(i,j) * speedofsound * speedofsound)
            mass(i,j) = rho_total(i,j) ! *acceleration_g
            Force_x(i,j) = 0.0
            Force_y(i,j) = acceleration_g * mass(i,j)
            
            do k=0,8
                f1(k,i,j)   = 0.0
                f2(k,i,j)   = 0.0
                feq1(k,i,j) = 0.0
                feq2(k,i,j) = 0.0
            enddo
            
            ff(0,i,j) = 0.0
            ff(1,i,j) = 0.0
            
        enddo
    enddo
    
    do i = 0, n
        do j = 0, m
            
            t1 = uu(i, j) * uu(i, j) + vv(i, j) * vv(i, j)
            ! feq1, feq2
            do k = 0, 8
                t2 = uu(i, j) * cx(k) + vv(i, j) * cy(k)
                feq1(k,i,j)      = rhoo_1*( C1(k) + w(k)*( 1 + t2*3 - 1.5*(t1*t1)+4.5*t2**2 ) )
                feq2(k,i,j)      = rhoo_2*( C2(k) + w(k)*( 1 + t2*3 - 1.5*(t1*t1)+4.5*t2**2 ) )
                f1(k,i,j) = feq1(k,i,j)
                f2(k,i,j) = feq2(k,i,j)
            enddo
        enddo
    enddo
    
    do i=0,n
        do j=0,m
            integral_mass = integral_mass + mass(i,j)
        enddo
    enddo
    print*, 'integral mass after step one = ', integral_mass
return
end

                        
subroutine debug_info(Force_x, Force_y, rho_total, step, n, m, mass, uu, vv, f1, f2)
   
real Force_x(0:n,0:m), Force_y(0:n, 0:m)  
real rho_total(0:n, 0:m)
real mass(0:n,0:m) 
real uu(0:n,0:m)
real vv(0:n,0:m)
real f1(0:8, 0:n, 0:m), f2(0:8, 0:n, 0:m)
integer i, j, k
integer step

! open(21,file='debug_info/Fx.dat')
! open(22,file='debug_info/Fy.dat')
! open(23,file='debug_info/eps.dat')
! open(24,file='debug_info/mass.dat')
! open(25,file='debug_info/u.dat')
! open(26,file='debug_info/v.dat')
! 27 - distrib functi
! 5 -> evolution rho

! open(30,file='debug_info/Df_0.dat')
! open(31,file='debug_info/Df_1.dat')
! open(32,file='debug_info/Df_2.dat')
! open(33,file='debug_info/Df_3.dat')
! open(34,file='debug_info/Df_4.dat')
! open(35,file='debug_info/Df_5.dat')
! open(36,file='debug_info/Df_6.dat')
! open(37,file='debug_info/Df_7.dat')
! open(38,file='debug_info/Df_8.dat')

     write(21,*) 'step', step
     write(22,*) 'step', step
     write(23,*) 'step', step
     write(24,*) 'step', step
     write(25,*) 'step', step
     write(26,*) 'step', step
     write(27,*) 'step', step
     do i=0,n
	    do j=0,m
            write(5,*)  rho_total(i,j)
            write(21,*) Force_x(i,j)
            write(22,*) Force_y(i,j)
            write(24,*) mass(i,j)
            write(25,*) uu(i,j)
            write(26,*) vv(i,j)
	        do k=0,8
                write(27,*) 'k=', k, f1(k,i,j)
            enddo
            
            write(30,*)  f1(0, i,j)
            write(31,*)  f1(1, i,j)
            write(32,*)  f1(2, i,j)
            write(33,*)  f1(3, i,j)
            write(34,*)  f1(4, i,j)
            write(35,*)  f1(5, i,j)
            write(36,*)  f1(6, i,j)
            write(37,*)  f1(7, i,j)
            write(38,*)  f1(8, i,j)
            
            write(40,*)  f2(0, i,j)
            write(41,*)  f2(1, i,j)
            write(42,*)  f2(2, i,j)
            write(43,*)  f2(3, i,j)
            write(44,*)  f2(4, i,j)
            write(45,*)  f2(5, i,j)
            write(46,*)  f2(6, i,j)
            write(47,*)  f2(7, i,j)
            write(48,*)  f2(8, i,j)
            
        end do
     end do
  !   pause
return
end

                     
subroutine collision(uu, vv, ff, f1, f2, feq1, feq2, rhoo_1, rhoo_2, omega_1, omega_2, w, cx, cy, n, m, theta, dt, step, substance_after_first_step, alpha_k1, alpha_k2, Psi)
real ff(0:1, 0 : n, 0 : m), f1(0:8, 0:n, 0:m), f2(0:8, 0:n, 0:m), previous_f(0 : 8, 0 : n, 0 : m)  !previous_f(0 : 8, 0 : n+2, 0 : m+2)
real feq1(0 : 8, 0 : n, 0 : m), feq2(0 : 8, 0 : n, 0 : m)
real rhoo_1, rhoo_2, C1(0  : 8), C2(0  : 8), B(0:8), alpha_k1, alpha_k2, Ak1, Ak2 ! rho(0 : n, 0 : m)
real Psi (0:n, 0:m)
real second_collision_1(0:8,0:n,0:m), second_collision_2(0:8,0:n,0:m), ff_module, ff_cx_cy_scalar_multiplication
real w(0 : 8), cx(0 : 8), cy(0 : 8)
integer i, j, k, step, i_i, j_j
real uu(0 : n, 0 : m), vv(0 : n, 0 : m)
real theta 
real omega, f_sum, omega_1, omega_2, summ_ff_x, summ_ff_y
real delta ! constant that affects interaface thickness 
real current_amount_of_substance, substance_after_first_step
real t1, t2, t3, s1, s2, s3, tau1, tau2

Ak1=0.0001
Ak2=0.0001

tau1 = 1.0/omega_1
tau2 = 1.0/omega_2

t1 = 0.0
t2 = 0.0
t3 = 0.0
s1 = 0.0
s2 = 0.0
s3 = 0.0

delta = 0.98

rho_g = 1
f_sum = 0.0
second_collision_1 = 0.0
second_collision_2 = 0.0
!substance_after_first_step = 0.0

C1(0) = alpha_k1
C1(1) = (1-alpha_k1)/5.0
C1(2) = (1-alpha_k1)/5.0
C1(3) = (1-alpha_k1)/5.0
C1(4) = (1-alpha_k1)/5.0
C1(5) = (1-alpha_k1)/20.0
C1(6) = (1-alpha_k1)/20.0
C1(7) = (1-alpha_k1)/20.0
C1(8) = (1-alpha_k1)/20.0

C2(0) = alpha_k2
C2(1) = (1-alpha_k2)/5.0
C2(2) = (1-alpha_k2)/5.0
C2(3) = (1-alpha_k2)/5.0
C2(4) = (1-alpha_k2)/5.0
C2(5) = (1-alpha_k2)/20.0
C2(6) = (1-alpha_k2)/20.0
C2(7) = (1-alpha_k2)/20.0
C2(8) = (1-alpha_k2)/20.0

B(0) = - 4.0/27.0

B(1) =   2.0/27.0
B(2) =   2.0/27.0
B(3) =   2.0/27.0
B(4) =   2.0/27.0

B(5) =   5.0/108.0
B(6) =   5.0/108.0
B(7) =   5.0/108.0
B(8) =   5.0/108.0


! tau1, tau2 through omega1, omega2
s1 = 2* (tau1*tau2) / (tau1+tau2)
s2 = 2*(tau1-s1)/delta
s3 = -s2 / (2*delta)

t1 = s1
t2 = 2 * (t1-tau2)/delta
t3 = t2 / (2*delta)

    second_collision_1 = 0.0
    second_collision_2 = 0.0
    ! second-order collision operator, gradient DF. i=1..N, j=1..M
    do i = 0, n
        do j = 0, m

            ff_ff_x = 0.0
            ff_ff_y = 0.0 
            
            ! 1
            summ_ff_y = 0.0
            summ_ff_x = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (   (f1(k, i+1, j) - f2(k, i+1, j) ) )
                summ_ff_y = summ_ff_y + (   (f1(k, i+1, j) - f2(k, i+1, j) ) )
            enddo
            ff_ff_x = cx(1) * summ_ff_x
            ff_ff_y = cy(1) * summ_ff_y
            
            ! 2
            summ_ff_y = 0.0
            summ_ff_x = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (   (f1(k, i, j+1) - f2(k, i, j+1) ) )
                summ_ff_y = summ_ff_y + (   (f1(k, i, j+1) - f2(k, i, j+1) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(2) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(2) * summ_ff_y
            
            ! 3
            summ_ff_x = 0.0
            summ_ff_y = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (  (f1(k, i-1, j) - f2(k, i-1, j) ) )
                summ_ff_y = summ_ff_y + (  (f1(k, i-1, j) - f2(k, i-1, j) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(3) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(3) * summ_ff_y
            
            ! 4
            summ_ff_x = 0.0
            summ_ff_y = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (  cx(k) * (f1(k, i, j-1) - f2(k, i, j-1) ) )
                summ_ff_y = summ_ff_y + (  cy(k) * (f1(k, i, j-1) - f2(k, i, j-1) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(4) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(4) * summ_ff_y
            
            ! 5
            summ_ff_x = 0.0
            summ_ff_y = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (  cx(k) * (f1(k, i+1, j+1) - f2(k, i+1, j+1) ) )
                summ_ff_y = summ_ff_y + (  cy(k) * (f1(k, i+1, j+1) - f2(k, i+1, j+1) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(5) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(5) * summ_ff_y
            
            ! 6
            summ_ff_x = 0.0
            summ_ff_y = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (  cx(k) * (f1(k, i-1, j+1) - f2(k, i-1, j+1) ) )
                summ_ff_y = summ_ff_y + (  cy(k) * (f1(k, i-1, j+1) - f2(k, i-1, j+1) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(6) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(6) * summ_ff_y
            
            ! 7
            summ_ff_x = 0.0
            summ_ff_y = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (  cx(k) * (f1(k, i-1, j-1) - f2(k, i-1, j-1) ) )
                summ_ff_y = summ_ff_y + (  cy(k) * (f1(k, i-1, j-1) - f2(k, i-1, j-1) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(7) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(7) * summ_ff_y
            
            
            ! 8
            summ_ff_x = 0.0
            summ_ff_y = 0.0
            do k = 0, 8
                summ_ff_x = summ_ff_x + (  cx(k) * (f1(k, i+1, j-1) - f2(k, i+1, j-1) ) )
                summ_ff_y = summ_ff_y + (  cy(k) * (f1(k, i+1, j-1) - f2(k, i+1, j-1) ) )
            enddo
            ff_ff_x = ff_ff_x + cx(8) * summ_ff_x
            ff_ff_y = ff_ff_y + cy(8) * summ_ff_y
            
            
            ff(0,i,j) = ff_ff_x
            ff(1,i,j) = ff_ff_y
            
            ff_module = sqrt(  (  ff(0,i,j)*ff(0,i,j)+ff(1,i,j)*ff(1,i,j) )      )
            
            do k = 0, 8 
                ff_cx_cy_scalar_multiplication = cx(k)*ff(0,i,j)+cy(k)*ff(1,i,j)
                if (ff_module.lt.0.00001) then
                    second_collision_1(k,i,j) = 0.5*Ak1*ff_module* ( - B(k) )
                    second_collision_2(k,i,j) = 0.5*Ak2*ff_module* ( - B(k) )
                else
                    second_collision_1(k,i,j) = 0.5*Ak1*ff_module * ( w(k) * (ff_cx_cy_scalar_multiplication**2 / ff_module**2) - B(k) )
                    second_collision_2(k,i,j) = 0.5*Ak2*ff_module * ( w(k) * (ff_cx_cy_scalar_multiplication**2 / ff_module**2) - B(k) )
                endif
                
            enddo
            
        enddo
    enddo

    
    ! Df1, Df2, feq1, feq2
    do i = 0, n
        do j = 0, m
            
            t1 = uu(i, j) * uu(i, j) + vv(i, j) * vv(i, j)
            ! feq1, feq2
            do k = 0, 8
                t2 = uu(i, j) * cx(k) + vv(i, j) * cy(k)
                feq1(k,i,j) = rhoo_1*( C1(k) + w(k)*( 1 + t2*3 - 1.5*(t1*t1)+4.5*t2**2 ) )
                feq2(k,i,j) = rhoo_2*( C2(k) + w(k)*( 1 + t2*3 - 1.5*(t1*t1)+4.5*t2**2 ) )
            enddo
            
            ! adjust omega
            if (Psi(i,j)>delta) then 
                omega = omega_1
            elseif(Psi(i,j)>0.and.Psi(i,j)<=delta) then
                omega = 1 / ( s1 + s2*Psi(i,j) + s3*Psi(i,j)*Psi(i,j) ) 
            elseif(Psi(i,j) <= 0.and.Psi(i,j) >= -delta)    then
                omega = 1 / (t1 + t2*Psi(i,j) + t3*Psi(i,j)*Psi(i,j)  ) 
            elseif(Psi(i,j) < -delta) then
                omega = omega_2
            endif
            
            do k = 0, 8
                f1(k,i,j) = f1(k,i,j) - omega*( f1(k,i,j) - feq1(k,i,j) ) + second_collision_1(k,i,j) 
                f2(k,i,j) = f2(k,i,j) - omega*( f2(k,i,j) - feq2(k,i,j) ) + second_collision_2(k,i,j) 
            enddo
            
        enddo
    enddo
    
    
return
end


subroutine recoloring(ff, f1, f2, rhoo_1, rhoo_2, rho_total, cx, cy, alpha_k, n, m, which)    
real ff(0:1,0:n,0:m), f1(0:8,0:n,0:m), f2(0:8,0:n,0:m),f(0:8,0:n,0:m), sum_f
real rhoo_1, rhoo_2, rho_total(0:n,0:m)
real cx(0:8), cy(0:8)
real module_cx_cy, ff_module, ff_cx_cy_scalar_multiplication
real beta
real C(0:8), alpha_k
integer which

beta = 0.5

C(0) = alpha_k
C(1) = (1-alpha_k)/5.0
C(2) = (1-alpha_k)/5.0
C(3) = (1-alpha_k)/5.0
C(4) = (1-alpha_k)/5.0
C(5) = (1-alpha_k)/20.0
C(6) = (1-alpha_k)/20.0
C(7) = (1-alpha_k)/20.0
C(8) = (1-alpha_k)/20.0

   ! module_cx_cy = 0.0
   ! do k = 0, 8
   !     module_cx_cy = module_cx_cy + (cx(k)**2+ cy(k)**2)
   ! enddo
   ! module_cx_cy = sqrt(module_cx_cy)

    do i = 0, n
        do j = 0, m
            
            sum_f = 0.0
            !do k = 0, 8
            !    sum_f = sum_f + f(k,i,j)
            !enddo
            
            ff_module = 0.0
            
            !ff_cx_cy_scalar_multiplication = 0.0
            !do k = 0,8
                !ff_module = f_module + (ff(0, k, i, j)**2+ ff(1, k, i, j)**2)
                !ff_cx_cy_scalar_multiplication = cx(k)*ff(0, k, i, j) + cy(k)*ff(1, k, i, j)
            !enddo
            ff_module = sqrt(  (  ff(0,i,j)*ff(0,i,j)+ff(1,i,j)*ff(1,i,j) )      )
            if (ff_module < 0.00000001) then
                do k = 0, 8
                    sum_f = f1(k,i,j) + f2(k,i,j)
                    f(k,i,j) = (rhoo_1 / rho_total(i,j)) * sum_f
                enddo
            else
                module_cx_cy = 0.0
                do k = 0, 8
                    sum_f = f1(k,i,j) + f2(k,i,j)
                    ff_cx_cy_scalar_multiplication = cx(k)*ff(0, i, j)+cy(k)*ff(1, i, j)
                    module_cx_cy = sqrt(  (cx(k)**2 + cy(k)**2)   )
                    ! feq(rho, u=0) = C1(i) * rho_total
                    if (which == 1) then
                        f(k,i,j) = (rhoo_1 / rho_total(i,j)) * sum_f + &
                        beta * ( (rhoo_1 * rhoo_2) / rho_total(i,j)**2) * ( C(k) * rho_total(i,j)) &
                        * cos( ff_cx_cy_scalar_multiplication / ( module_cx_cy * ff_module ) )    
                    else
                        f(k,i,j) = (rhoo_2 / rho_total(i,j)) * sum_f - &
                        beta * ( (rhoo_1 * rhoo_2) / rho_total(i,j)**2) * ( C(k) * rho_total(i,j)) &
                        * cos( ff_cx_cy_scalar_multiplication / ( module_cx_cy * ff_module ) )   
                    endif
                    
                enddo
            endif
            
        enddo
    enddo
    
   do i = 0, n
       do j = 0, m
           do k = 0, 8
               
               if (which == 1) then
                   f1(k,i,j) = f(k,i,j)
               else
                   f2(k,i,j) = f(k,i,j)
               endif
               
           enddo
       enddo
   enddo
        
    
return
end
    

subroutine streaming(f,n,m)
real f(0:8,0:n,0:m)
integer i, j

	    ! streaming
	    DO j=0,m
		    DO i=n,1,-1 ! RIGHT TO LEFT
			    f(1,i,j)=f(1,i-1,j)
		    END DO
	
		    DO i=0,n-1  ! LEFT TO RIGHT
			    f(3,i,j)=f(3,i+1,j)
		    END DO
	
	    END DO

	    DO j=m,1,-1 !TOP TO BOTTOM
		    DO i=0,n
			    f(2,i,j)=f(2,i,j-1)
		    END DO
		    DO i=n,1,-1
			    f(5,i,j)=f(5,i-1,j-1)
		    END DO
		    DO i=0,n-1
			    f(6,i,j)=f(6,i+1,j-1)
		    END DO
	    END DO
	
	    DO j=0,m-1 !BOTTOM TO TOP
		
		    DO i=0,n
			    f(4,i,j)=f(4,i,j+1)
		    END DO
		
		    DO i=0,n-1
			    f(7,i,j)=f(7,i+1,j+1)
		    END DO
		
		    DO i=n,1,-1
			    f(8,i,j)=f(8,i-1,j+1)
		    END DO
        END DO

return
end    

                                
subroutine boundary_conditions(f, n, m, uu, vv)
real f(0:8,0:n,0:m)
real uu(0:n,0:m), vv(0:n,0:m)
integer i, j
     
    ! top wall, no-slip
	do i=0,n
           f(7,i,m) = f(5,i,m)
           f(4,i,m) = f(2,i,m)
           f(8,i,m) = f(6,i,m)
    enddo
    
    ! bottom wall, no-slip
    do i=0,n
           f(6,i,0) = f(8,i,0)
           f(2,i,0) = f(4,i,0)
           f(5,i,0) = f(7,i,0)
    enddo
    
    ! west wall, no-slip
	do j=0,m
	       f(5,0,j) = f(7,0,j)
           f(1,0,j) = f(3,0,j)
           f(8,0,j) = f(6,0,j)
    enddo
    
    ! east wall, no-slip
	 do j=0,m
	       f(6,n,j) = f(8,n,j)
           f(3,n,j) = f(1,n,j)
           f(7,n,j) = f(5,n,j)
     enddo
    
return
end    

                                        
subroutine rho_and_velocity_calculation(f1,f2, rho_total, uu, vv ,cx, cy, n, m, Force_x, Force_y, step, dt)
real f1(0:8,0:n,0:m),cx(0:8),cy(0:8), f2(0:8,0:n,0:m)
real ssum,usum,vsum
real Force_x(0:n,0:m), Force_y(0:n, 0:m)
real rho_total(0:n, 0:m)
real uu(0:n,0:m), vv(0:n,0:m)
integer step
real dt
real modul_u
integer i, j
write(14,*)"step", step


	    do j=0,m
		    do i=0,n
			    ssum=0.0
                
				do k=0,8
                    ssum=ssum+ f1(k,i,j) + f2(k,i,j)
                    !write(14,*) 'i=',i,'j=',j,k, f(k,i,j)
                enddo
                rho_total(i,j)=ssum
                
		    enddo
	    enddo
        
            DO i=0,n
		        DO j=0,m
			        usum=0.0
			        vsum=0.0
				    DO k=0,8
					    usum = usum + f1(k,i,j)*cx(k) + f2(k,i,j)*cx(k)
					    vsum = vsum + f1(k,i,j)*cy(k) + f2(k,i,j)*cy(k)
                    END DO
                    
                    if (rho_total(i,j).ne.0) then    
                        uu(i,j) = usum
                        vv(i,j) = vsum  + (Force_y(i,j)*dt*0.5)/rho_total(i,j)
                    else
                        uu(i,j) = 0.0
                        vv(i,j) = 0.0
                    endif
                    
                    if (uu(i,j) .ne. uu(i,j)) then
                         print*, "Divergence rhouv function "
                         print*, rho_total(i,j), Force_x(i,j), Force_y(i,j), step
                         pause
                    endif
                    
			        !vv(i,j) = vsum / rho(i,j) + Fy(i,j)/2.0
		        END DO
            END DO
            
	    do j=0,m
            vv(n,j)=0.0
            uu(n,j)=0.0
            vv(0,j)=0.0
            uu(0,j)=0.0
        enddo
        
        do i=0, n
            vv(i,0)=0.0
            uu(i,0)=0.0
            vv(i,m)=0.0
            uu(i,m)=0.0
        enddo
return
end    
    

subroutine force_calculation(n, m, Force_x, Force_y, step, dx, cx, cy, mass, acceleration_g, Psi, w, rho_total)
real mass(0:n, 0:m)  , Psi(0:n, 0:m) , w(0 : 8), rho_total(0:n, 0:m)
real Force_x(0:n, 0:m), Force_y(0:n, 0:m), cx(0:8),cy(0:8), G(0:8)
integer step
real acceleration_g
real A, G1, G2
real dx
integer i, j
real sum_f
G1 = 1.0
G2 = 1.0

    do i=0, n
        do j = 0, m
            Force_x(i,j) = 0.0
            Force_y(i,j) = rho_total(i,j) * acceleration_g
           
        enddo
    enddo

return
end    
    
    
subroutine psi_calculation(n, m, step, rho_f1, rho_f2, Psi)
real Psi(0:n,0:m), rho_f1(0:n,0:m), rho_f2(0:n,0:m)
integer i,j,step
    
    do i = 0, n
        do j = 0, m
            Psi(i,j) = (  rho_f1(i,j) - rho_f2(i,j) ) / (rho_f1(i,j) + rho_f2(i,j))
        enddo
    enddo
    
return 
end
    

subroutine result(uu,vv,u0,n,m,rho)
real uu(0:n,0:m),vv(0:n,0:m), rho(0:n,0:m)
real strf(0:n,0:m)
integer i, j
!open(5,file='streamt.dat')

! streamfunction calculations
 !   strf(0,0)=0.
 !   do i=0,n
 !       
!	    rhoav=0.5*(rho1(i-1,0)+rho1(i,0))
!	    if(i.ne.0) strf(i,0)=strf(i-1,0)-rhoav*0.5*(v1(i-1,0)+v1(i,0))
!	    do j=1,m
!	        rhom=0.5*(rho1(i,j)+rho1(i,j-1))
!	        strf(i,j)=strf(i,j-1)+rhom*0.5*(u1(i,j-1)+u1(i,j))
!       end do
!        
!    end do
    
    ! output Ux, Uy
    do i=0,n
	    do j=0,m
            write(7,*) rho(i,j)
	        write(8,*) uu(i,j)
	        write(9,*) vv(i,j)
	    end do
    end do
    
    ! output streamfunction
!	do i=0,n
!	    do j=0,m

!	    enddo
!    enddo
    
    ! output module velocity
!    do i=0,n
!	    do j=0,m

!	    enddo
!    enddo 

return
    end
    
    
subroutine close_files()
!close(1)

close(2)
close(3)
close(4)
close(5)
close(7)
close(8)
close(9)
close(21)
close(22)
close(23)
close(24)
close(25)
close(26)
close(27)
close(30)
close(31)
close(32)
close(33)
close(34)
close(35)
close(36)
close(37)
close(38)

return 
end
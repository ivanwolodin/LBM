	program thermalconvection
C natural thermalgravity convection
	parameter (n=100,m=100)
	real*8 f(0:8,0:n,0:m)                                               ! �㭪�� ��।������ ��� ᪮���
	real*8 feq(0:8,0:n,0:m),rho(0:n,0:m)                                ! ࠢ����᭠� �㭪�� ��।������  ��� ᪮��� � �㭪�� ���⭮��
	real*8 w(0:8), cx(0:8),cy(0:8)                                      ! ��ᮢ� �����樥��� ��⪨ � ࠧ�襭�� ���祭�� ᪮���
	real*8 u(0:n,0:m), v(0:n,0:m), tw, gbeta                            ! ���ᨢ ��� ���� ᪮��� �� ��� � �� ��४,
	real*8 g(0:8,0:n,0:m), geq(0:8,0:n,0:m),th(0:n,0:m)                 ! �㭪�� ��।������ ��� ⥬�������, ࠢ����᭠� �㭪�� ��।������ ��� ⥬�������, ���ᨢ ��� ⥬�����୮�� ����
	integer i,kkk
	open(2,file='ufield.dat')
	open(20,file='vfield.dat')
	open(3,file='uvely.dat')
	open(4,file='vvelx.dat')
!
	cx(:)=(/0.0,1.0,0.0,-1.0,0.0,1.0,-1.0,-1.0,1.0/)
	cy(:)=(/0.0,0.0,1.0,0.0,-1.0,1.0,1.0,-1.0,-1.0/)
	w(:)=(/4./9.,1./9.,1./9.,1./9.,1./9.,1./36.,1./36.,1./36.,1./36./)
	uo=0.0
	sumvelo=0.0
	rhoo=6.00        !   ? ��ନ�஢�� �� 6?
	dx=1.0           ! 蠣 �⪨
	dy=dx            ! 蠣 �⪨
	dt=1.0           ! �६����� 蠣
	tw=1.0           !       ���. ���. ⥬���
	th=0.0           !       ?
	ra=1.0e5         ! �᫮ �����
	pr=0.71          ! �᫮ �࠭���
	visco=0.02       ! �離����
	alpha=visco/pr   ! ⥬�����ய஢�������
	pr=visco/alpha
	gbeta=ra*visco*alpha/(float(m*m*m))    !        ?  float?
	Re=uo*m/alpha
	print *, 'Re=', Re
	omega=1.0/(3.*visco+0.5)        ! ���� �⮫�������� ����
	omegat=1.0/(3.*alpha+0.5)       ! ����         ?
	mstep=40000                     ! ������⢮ 蠣��
	kkk=0
	do j=0,m
	do i=0,n
	rho(i,j)=rhoo
	u(i,j)=0.0
	v(i,j)=0.0
	end do
	end do
	do i=0,n
	u(i,m)=uo
	v(i,m)=0.0                   ! ������ �㫥��� ᪮����� �� �ᥩ ��⪥
	end do
	! main loop
	do kk=1,mstep
	call collision(u,v,f,feq,rho,omega,w,cx,cy,n,m,th,gbeta)         ! ���᫥��� �㭪樨 ��।������ ��� ᪮���
	call streaming(f,n,m)                                            ! ������� ����������⢨� ��� ᪮��⭮� �㭪樨 ��।������
	call bounceb(f,n,m)                                              ! �࠭.�᫮��� ��� ᪮��⭮� �㭪樨 ��।������
	call rhouv(f,rho,u,v,cx,cy,n,m)                                  ! ���.���⭮�� � ������ 㧫� � ���� ᪮���
	! �����������
	! collision for scalar
	call collt(u,v,g,geq,th,omegat,w,cx,cy,n,m)                      ! ���. ⥬�����୮� �㭪樨 ��।������ � ⥬�����୮� ࠢ����᭮� �㭪樨 ��।������
	! streaming for scalar
	call streaming(g,n,m)                                            ! ������� ����������⢨� ��� ⥬�����୮� �㭪樨 ��।������
	call gbound(g,tw,w,n,m)                                          ! ������� �࠭.�᫮��� ��� ⥬�����୮� �㭪樨 ��।������
	do j=0,m
	do i=0,n
	sumt=0.0
	do k=0,8
	sumt=sumt+g(k,i,j)
	end do
	th(i,j)=sumt
	end do
	end do
C	print *, th(n/2,m/2),v(5,m/2),rho(0,m/2),u(n/2,m/2),v(n/2,m/2),rho(n,m/2)
	kkk=kkk+1
	if (kkk.eq.1000) then
	print*,kk
	kkk=0
	endif

	END DO
	! end of the main loop
	
	call result(u,v,rho,th,uo,n,m,ra)                                ! �뢮� १����
	
	call output(u,v,rho,th,uo,n,m,ra)
	
	stop
	end
	! end of the main program

        subroutine collision(u,v,f,feq,rho,omega,w,cx,cy,n,m,th,gbeta)
	real*8 f(0:8,0:n,0:m)
	real*8 feq(0:8,0:n,0:m),rho(0:n,0:m)
	real*8 w(0:8), cx(0:8),cy(0:8)
	real*8 u(0:n,0:m), v(0:n,0:m)
	real*8 th(0:n,0:m),tref,force,gbeta
	tref=0.50                                                          !    �⪫������ �� ࠢ������?
	DO i=0,n
	DO j=0,m
	t1=u(i,j)*u(i,j)+v(i,j)*v(i,j)
	DO k=0,8
	t2=u(i,j)*cx(k)+v(i,j)*cy(k)
	force=3.*w(k)*gbeta*(th(i,j)-tref)*cy(k)*rho(i,j)                  ! ����稥 ���� ⥬������� 㢥��稢��� ��⥭�ᨢ����� �⮫��������
	if(i.eq.0.or.i.eq.n) force =0.0
	if(j.eq.0.or.j.eq.m) force =0.0
	feq(k,i,j)=rho(i,j)*w(k)*(1.0+3.0*t2+4.50*t2*t2-1.50*t1)
	f(k,i,j)=omega*feq(k,i,j)+(1.-omega)*f(k,i,j)+force
	END DO
	END DO
	END DO
	return
	end
	subroutine collt(u,v,g,geq,th,omegat,w,cx,cy,n,m)
	real*8 g(0:8,0:n,0:m),geq(0:8,0:n,0:m),th(0:n,0:m)
	real*8 w(0:8),cx(0:8),cy(0:8)
	real*8 u(0:n,0:m),v(0:n,0:m)
	do i=0,n
	do j=0,m
	do k=0,8
	geq(k,i,j)=th(i,j)*w(k)*(1.0+3.0*(u(i,j)*cx(k)+v(i,j)*cy(k)))       ! ⥬������� �㭪樨 ��।������ ������� �� ᪮��� � ⥬�������
	g(k,i,j)=omegat*geq(k,i,j)+(1.0-omegat)*g(k,i,j)
	end do
	end do
	end do
	return
	end
	subroutine streaming(f,n,m)
	real*8 f(0:8,0:n,0:m)
	! streaming
	DO j=0,m
	DO i=n,1,-1 !RIGHT TO LEFT
	f(1,i,j)=f(1,i-1,j)
	END DO
	DO i=0,n-1 !LEFT TO RIGHT
	f(3,i,j)=f(3,i+1,j)
	END DO
	END DO
	DO j=m,1,-1 !TOP TO BOTTOM
	DO i=0,n
	f(2,i,j)=f(2,i,j-1)
	END DO
	DO i=n,1,-1
	f(5,i,j)=f(5,i-1,j-1)
	END DO
	DO i=0,n-1
	f(6,i,j)=f(6,i+1,j-1)
	END DO
	END DO
	DO j=0,m-1 !BOTTOM TO TOP
	DO i=0,n
	f(4,i,j)=f(4,i,j+1)
	END DO
	DO i=0,n-1
	f(7,i,j)=f(7,i+1,j+1)
	END DO
	DO i=n,1,-1
	f(8,i,j)=f(8,i-1,j+1)
	END DO
	END DO
	return
	end
	subroutine bounceb(f,n,m)
	real*8 f(0:8,0:n,0:m)
	do j=0,m
	!west boundary
	f(1,0,j)=f(3,0,j)
	f(5,0,j)=f(7,0,j)
	f(8,0,j)=f(6,0,j)
	!east boundary
	f(3,n,j)=f(1,n,j)
	f(7,n,j)=f(5,n,j)
	f(6,n,j)=f(8,n,j)
	end do
	do i=0,n
	!south boundary
	f(2,i,0)=f(4,i,0)
	f(5,i,0)=f(7,i,0)
	f(6,i,0)=f(8,i,0)
	!north boundary
	f(4,i,m)=f(2,i,m)
	f(8,i,m)=f(6,i,m)
	f(7,i,m)=f(5,i,m)
	end do
	return
	end
	subroutine gbound(g,tw,w,n,m)
	real*8 g(0:8,0:n,0:m)
	real*8 w(0:8),tw
	! Boundary conditions
	! West boundary condition, T=1.
	do j=0, m
	g(1,0,j)=tw*(w(1)+w(3))-g(3,0,j)
	g(5,0,j)=tw*(w(5)+w(7))-g(7,0,j)
	g(8,0,j)=tw*(w(8)+w(6))-g(6,0,j)
	end do
	! East boundary condition, T=0.
	do j=0, m
	g(6,n,j)=-g(8,n,j)
	g(3,n,j)=-g(1,n,j)
	g(7,n,j)=-g(5,n,j)
	end do
	! Top boundary conditions, Adiabatic
	do i=0,n
	g(8,i,m)=g(8,i,m-1)
	g(7,i,m)=g(7,i,m-1)
	g(6,i,m)=g(6,i,m-1)
	g(5,i,m)=g(5,i,m-1)
	g(4,i,m)=g(4,i,m-1)
	g(3,i,m)=g(3,i,m-1)
	g(2,i,m)=g(2,i,m-1)
	g(1,i,m)=g(1,i,m-1)
	g(0,i,m)=g(0,i,m-1)
	end do
	!Bottom boundary conditions, Adiabatic
	do i=0,n
	g(1,i,0)=g(1,i,1)
	g(2,i,0)=g(2,i,1)
	g(3,i,0)=g(3,i,1)
	g(4,i,0)=g(4,i,1)
	g(5,i,0)=g(5,i,1)
	g(6,i,0)=g(6,i,1)
	g(7,i,0)=g(7,i,1)
	g(8,i,0)=g(8,i,1)
	g(0,i,0)=g(0,i,1)
	end do
	return
	end
	subroutine tcalcu(g,th,n,m)
	real*8 g(0:8,0:n,0:m),th(0:n,0:m)
	do j=0,m
	do i=0,n
	ssumt=0.0
	do k=0,8
	ssumt=ssumt+g(k,i,j)
	end do
	th(i,j)=ssumt
	end do
	end do
	return
	end
	subroutine rhouv(f,rho,u,v,cx,cy,n,m)
	real*8 f(0:8,0:n,0:m),rho(0:n,0:m),u(0:n,0:m),v(0:n,0:m),cx(0:8),cy(0:8)
	do j=0,m
	do i=0,n
	ssum=0.0
	do k=0,8
	ssum=ssum+f(k,i,j)
	end do
	rho(i,j)=ssum
	end do
	end do
	DO i=0,n
	DO j=0,m
	usum=0.0
	vsum=0.0
	DO k=0,8
	usum=usum+f(k,i,j)*cx(k)
	vsum=vsum+f(k,i,j)*cy(k)
	END DO
	u(i,j)=usum/rho(i,j)
	v(i,j)=vsum/rho(i,j)
	END DO
	END DO
	return
	end
	subroutine result(u,v,rho,th,uo,n,m,ra)
	real*8 u(0:n,0:m),v(0:n,0:m),th(0:n,0:m)
	real*8 strf(0:n,0:m),rho(0:n,0:m)
	open(5,file='streamt.dat')
	open(50,file='temperature.dat')
	open(6,file='nuav.dat')
	! streamfunction calculations
	strf(0,0)=0.
	do i=0,n
	rhoav=0.5*(rho(i-1,0)+rho(i,0))
	if(i.ne.0) strf(i,0)=strf(i-1,0)-rhoav*0.5*(v(i-1,0)+v(i,0))
	do j=1,m
	rhom=0.5*(rho(i,j)+rho(i,j-1))
	strf(i,j)=strf(i,j-1)+rhom*0.5*(u(i,j-1)+u(i,j))
	end do
	end do
	! ������������
	do j=0,m
	do i=0,n
	write(2,*) u(i,j)
	write(20,*) v(i,j)
	end do
	end do

	do j=0,m
	write(3,*)j/float(m),u(5,j),u(n/2,j),u(n-10,j)
	end do
	do i=0,n
	write(4,*) i/float(n),th(i,m/2)
	end do
	do i=0,n
	do j=0,m
	write(5,*) i,j,strf(i,j)
	enddo
	enddo
	do i=0,n
	do j=0,m
	write(50,*)i,j,th(i,j)
	enddo
	enddo
	! Nusselt number Calculation
	snul=0.0
	snur=0.0
	do j=0,m
	rnul=(th(0,j)-th(1,j))*float(n)
	rnur=(th(n-1,j)-th(n,j))*float(n)
	snul=snul+rnul
	snur=snur+rnur
C	write(5,*)j/float(m),rnul,rnur
	end do
	avnl=snul/float(m)
	avnr=snur/float(m)
	write(6,*)ra,avnl,avnr
	return
	end
	
	
	subroutine output(u,v,rho,th,uo,n,m,ra)
        real*8 u(0:n,0:m),v(0:n,0:m),th(0:n,0:m)
	real*8 strf(0:n,0:m),rho(0:n,0:m)
	open(11,file='temperature.txt')
	open(111,file='ufield.txt')
	open(1111,file='vfield.txt')
	
	do j=0,m
             	do i=0,n
               	write(111,*) u(i,j)
	        write(1111,*) v(i,j)
	        end do
	end do
	
	do j=0,m
        	do i=0,n
          	write(11,*) th(i,j)
        	end do
	end do
	return
	end

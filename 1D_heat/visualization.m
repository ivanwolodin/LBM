clc;clear;
fileI = fopen('velocity_field','r');
out = fscanf(fileI,'%f');
fclose(fileI);
V = reshape(out,101,[]);

imagesc(V);

colorbar;
set(gca,'YDir','normal')